#include"Customer.h"
#include<map>
#include<ctime>

//choices
#define NO_CHOICE -1
#define SIGN_CUSTOMER 1
#define UPDATE_CUSTOMER 2
#define PRINT_MOST_PAY 3
#define EXIT 4

#define ADD_ITEM 1
#define RM_ITEM 2
#define RETURN 3

#define WAIT_TIME 900
#define ITEM_LIST_SIZE 10

//function declaration
int printMenu();
int printEditMenu();
void wait(long int dur);
void printItemList(Item* arr, int listSize);
void printItemList(std::set<Item> items);
int getChoice();
void buy(std::map<string, Customer>& customerList, Item* itemList, string customerName);
void editData(std::map<string, Customer>& customerList, Item* itemList, string customerName);


int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	map<string, Customer> abcCustomers;
	Item itemList[ITEM_LIST_SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	//the main program code
	int choice = NO_CHOICE;
	while (choice != EXIT)
	{
		choice = printMenu();

		//perform action according the choice of the user
		switch (choice)
		{
			case SIGN_CUSTOMER:
			{
				system("cls");
				std::cout << "Signing customer..." << std::endl;

				std::cout << "Please enter customer name: ";
				string customerName;
				//ignore the '\n' in the input buffer so the getline can work and the customer name \
				can now contain spaces 
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::getline(std::cin, customerName); //get the name of the customer

				Customer temp(customerName); //make temp customer for adding later

				std::map<string, Customer>::iterator it = abcCustomers.find(customerName);
				if (it != abcCustomers.end()) //if found the customer already
				{
					//print error message for WAIT_TIME ms and than go back to the main menu
					std::cout << "This name is taken - Customer already in the system" << std::endl;
					wait(WAIT_TIME);
					break;
				}
				else //customer name OK - add it to the system
				{
					abcCustomers.insert(std::pair<string, Customer>(temp.getName(), temp));
				}

				//now show the customer the item list
				buy(abcCustomers, itemList, customerName);
				break;
			}
			case UPDATE_CUSTOMER:
			{
				system("cls");
				std::cout << "Updating customer data..." << std::endl;

				std::cout << "Please enter customer name: ";
				string customerName;
				//ignore the '\n' in the input buffer so the getline can work and the customer name \
				can now contain spaces 
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::getline(std::cin, customerName); //get the name of the customer

				//iterator of find result of the customer name in the map
				std::map<string, Customer>::iterator it = abcCustomers.find(customerName);
				if (it == abcCustomers.end()) //if found the customer already
				{
					//print error message for WAIT_TIME ms and than go back to the main menu
					std::cout << "No customer with this name" << std::endl;
					wait(WAIT_TIME);
					break;
				}

				//show the items the customer bought
				std::cout << "The items on " << customerName << "'s list" << std::endl;
				printItemList(abcCustomers.at(customerName).getItems());

				editData(abcCustomers, itemList, customerName);
				break;
			}
			case PRINT_MOST_PAY:
			{
				if (abcCustomers.size() != 0)
				{
					double maxPay = 0;
					string maxName = abcCustomers.begin()->second.getName(); //equal to the first name
					map<string, Customer>::iterator it = abcCustomers.begin();
					for (it; it != abcCustomers.end(); it++)
					{
						if (it->second.totalSum() > maxPay)
						{
							maxName = it->second.getName();
							maxPay = it->second.totalSum();
						}
					}
					cout << "The customer who pays the most is: " << maxName << " and the total payment is: " << maxPay << std::endl;
					system("pause");
				}
				else
				{
					std::cout << "No customers signed" << endl;
					wait(WAIT_TIME);
				}
				break;
			}
			default:
				break;
		}

		system("cls"); //clear the screen
	}

	return 0;
}

/*
printMenu - prints the menu to the screen and get the choice
input: none
output: int - the choice of the user
*/
int printMenu()
{
	int choice = NO_CHOICE;
	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1.  Sign as a customer and by items." << std::endl;
	std::cout << "2.  Update existing customer's items." << std::endl;
	std::cout << "3.  Print the customer who pays the most." << std::endl;
	std::cout << "4.  Exit." << std::endl;
	std::cout << "Please enter your choice: ";
	choice = getChoice();

	if (!(choice >= SIGN_CUSTOMER && choice <= EXIT))
	{
		choice = NO_CHOICE; //the user didn't chose any specific option - reset to no choice
	}

	return choice;
}

/*
PrintEditMenu
input: none
output: int - the choice of the user
*/
int printEditMenu()
{
	int choice = NO_CHOICE;
	std::cout << "What would you like to do?" << std::endl;
	std::cout << "1.  Add items to the list." << std::endl;
	std::cout << "2.  Remove items from the list." << std::endl;
	std::cout << "3.  Return the the main menu." << std::endl;
	std::cout << "Please enter your choice: ";
	choice = getChoice();

	if (!(choice >= ADD_ITEM && choice <= RETURN))
	{
		choice = NO_CHOICE; //the user didn't chose any specific option - reset to no choice
	}

	return choice;
}

/*
wait function for more nice interface
input: long int - the duration (in ms) for the wait
output: none
*/
void wait(long int dur)
{
	int startTime = std::clock();
	while (std::clock() - startTime < dur)
	{
		//do nothing -- wait
	}
}

/*
printItemList - print the items on the itemList in the main
input: ItemList
output: none
*/
void printItemList(Item* arr, int listSize)
{
	std::cout << "Item list format: " << std::endl << "Item's name" << ", " << "Item's price" << std::endl;
	for (int i = 0; i < listSize; i++)
	{
		std::cout << i + 1 << ".  " << arr[i].getName() << ", " << arr[i].getPrice() << ", " << std::endl;
	}
}

/*
overload of the printItemList function - for a list of items from a set and not array
input: the set of items from a customer
output: none
*/
void printItemList(std::set<Item> items)
{
	//print all items in the set
	int i = 1;
	std::cout << "Item list format: " << "Name, Price, Count" << std::endl;
	for (std::set<Item>::iterator it = items.begin(); it != items.end(); it++)
	{
		Item temp = *it;
		std::cout << i << ".  " << temp.getName() << ", " << temp.getPrice() << ", " << temp.getCount() << std::endl;
		i++;
	}
}

/*
get number for choice which is only a number - with input filtering - does not accept letters
input: none
output: int the choice
*/
int getChoice()
{
	string sChoice;
	int choice;
	cin >> sChoice;
	try
	{
		choice = std::stoi(sChoice);
	}
	catch (exception& e)
	{
		choice = NO_CHOICE;
	}
	return choice;
}

/*
Buy - print items menu and add it to the customers list
input: 
	customersMap - the map of the customers
	Item* itemList - the array of the items available for buying
	string customerName - the name of the customer
output: none
*/
void buy(std::map<string, Customer>& customerList, Item* itemList, string customerName)
{
	int choice = NO_CHOICE;
	std::cout << "Which items would you like to buy: " << std::endl;
	printItemList(itemList, ITEM_LIST_SIZE);
	std::cout << "Press on the number of the item you want to buy and than press enter." << std::endl;
	std::cout << "To stop buying items press 0" << std::endl;
	while (choice != 0)
	{
		choice = getChoice();
		if (choice != NO_CHOICE && choice > 0 && choice <= ITEM_LIST_SIZE) //if not requested to stop add the item to the list of this customer
		{
			customerList.at(customerName).addItem(itemList[choice - 1]); //-1 for the index in the list printed starts from 1 and not from 0
		}
	}
}

/*
EditData - print the menu for option two and perform the specified action
input:
	customersMap - the map of the customers
	Item* itemList - the array of the items available for buying
	string customerName - the name of the customer
output: none
*/
void editData(std::map<string, Customer>& customerList, Item* itemList, string customerName)
{
	//print the menu to the user
	int choice = printEditMenu();

	switch (choice)
	{
		case ADD_ITEM:
		{
			system("cls");
			buy(customerList, itemList, customerName); //buy new items just like option one
			break;
		}
		case RM_ITEM:
		{	
			while (choice != 0)
			{
				system("cls");
				printItemList(customerList.at(customerName).getItems());
				std::cout << "Which item would you like to remove: " << std::endl;
				std::cout << "Press 0 to stop." << std::endl;

				set<Item> currentItems = customerList.at(customerName).getItems();
				set<Item>::iterator it = currentItems.begin();

				if (it != currentItems.end())
				{
					choice = getChoice();

					if (choice - 1 < currentItems.size())
					{
						for (int i = 0; i < choice - 1; i++)
						{
							it++; //increase it by one choice times
						}
						if (choice != NO_CHOICE && choice != 0)
						{
							customerList.at(customerName).removeItem(*it);
						}
					}
				}
				else
				{
					system("cls");
					cout << "Your list is empty" << std::endl;
					wait(WAIT_TIME); //wait
					break; //the list is empty - no need to stay there
				}
			}
			break;
		}
		default:
			break;
	}
}