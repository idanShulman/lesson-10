#include "Customer.h"

static int defaultNum = 1;

/*
Constructor
input: string - the name of the customer
output:none
*/
Customer::Customer(string name) : _name(name)
{
	if (name.length() == 0) //if got an empty string get a name of the type "Customer IndexNumber"
	{
		//get the new name using the defaultNum variable
		string newName = "Customer " + std::to_string(defaultNum);
		_name = newName; //set the name to "customer 1" but the next call the name will be "customer 2"
		defaultNum++; //increase the defaultNum by one for the next call
	}
}

/*
Empty constructor
input: none
output: none
*/
Customer::Customer()
{
	//get the new name using the defaultNum variable
	string newName = "Customer " + std::to_string(defaultNum);
	_name = newName; //set the name to "customer 1" but the next call the name will be "customer 2"
	defaultNum++; //increase the defaultNum by one for the next call
}

/*
TotalSum - return the total price of a customer to pay
input: none
output: none
*/
double Customer::totalSum() const
{
	//! this is the calculation with the iterators of the _items set
	/* - normal algorithm - not necessary because of the use of accumulate
	double total = 0; //set var for the summing process
	for (std::set<Item>::iterator i = this->_items.begin(); i != this->_items.end(); i++)
	{
		total += i->totalPrice();
	}
	return total;
	*/

	//! this is the calculation with the accumulate function from <numeric>
	//! with lambda expression for getting the total price for the accumulation
	//calculate the total sum of price the customer should pay with the accumulate function from <numeric>
	double total = std::accumulate(this->_items.begin(), this->_items.end(), 0.0, [](double accumulator, const Item& nextItem)
	{
		return accumulator + nextItem.totalPrice();
	});

	return total; //return the sum calculated
}

/*
addItem - adding an item to the itemList
input: item - the new item
output: none
*/
void Customer::addItem(Item newItem)
{
	//! if not found it will be set to set::end (_items.end()), if so - inset it, if not add count by one
	std::set<Item>::iterator it = _items.find(newItem); //the iterator to the new item if already found
	if (it != _items.end()) //if already found this 
	{
		Item temp = *it; //set temp to item containing the data it points to
		temp.setCount(temp.getCount() + 1); //increase the count of an item in the items set
		this->_items.erase(newItem); //delete previous item with not-updated count value
		this->_items.insert(temp); //inset the temp Item which is the the same item with update count value
	}
	else //was not found (it is equal to set::end) - inset the new item
	{
		_items.insert(newItem); //adding the new item to the items set
	}
}

/*
remmoveItem - removes an item from the set
input: the item to remove
output: none
*/
void Customer::removeItem(Item delItem)
{
	std::set<Item>::iterator it = _items.find(delItem); //the iterator to the new item if found
	if (it != _items.end()) //if found the item
	{
		Item temp = *it; //the temp item is containing the data of the item found

		if (temp.getCount() > 1) //if have two of the same item
		{
			temp.setCount(temp.getCount() - 1); //decrease the count of the temp item
			this->_items.erase(delItem); //delete previous item with old count value
			this->_items.insert(temp); //insert the new temp item with the updated count value
		}
		else //if it does not contain two of the same item (count = 1) - delete it from the set
		{
			this->_items.erase(delItem); //removes the item from the items set
		}
	}
}

/*
Getter - return the name of the customer
input: none
output: string - the name
*/
string Customer::getName()
{
	return _name;
}

/*
Getter - return current copy of the items set
(copy because it is returning by value and not a pointer or a reference)
input: none
output: set<Item> - the set of items
*/
set<Item> Customer::getItems()
{
	return _items;
}

/*
Setter - set the name of the customer
input: string newName - the new name for the customer
output: none
*/
void Customer::setName(string newName)
{
	if (newName !+ "")
	{
		_name = newName;
	}
	else
	{
		std::cout << "Name must no be empty" << std::endl;
	}
}

/*
Setter - set the items to another set of items
input: set<Item> newItemSet - the new set
output: none
*/
void Customer::setItems(set<Item> newItemSet)
{
	_items = newItemSet;
}
