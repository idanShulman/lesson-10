#pragma once
#include"Item.h"
#include<set>
#include<numeric>
#include<string>

#define ZERO_CHAR '0'

class Customer
{
public:
	Customer(string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	string getName();
	set<Item> getItems();

	void setName(string newName);
	void setItems(set<Item> newItemSet);

private:
	string _name;
	set<Item> _items;


};
