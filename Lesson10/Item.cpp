#include "Item.h"

/*
Constructor
input: name, serial number and the price of an item
output: none
*/
Item::Item(string name, string serial, double price) : _name(name), _serialNumber(serial), _unitPrice(price)
{
	_count = 1; //default value of the count var of an item is one.
}

/*
Destructor
input: none
output: none
*/
Item::~Item()
{
	//no need for anything - no dynamic allocation.
}

/*
totalPrice - return the total price of an item
input: none
output: double - the total price
*/
double Item::totalPrice() const
{
	return _count * _unitPrice; //return the total price for this item.
}

/*
Operator < - perform the operator on the string of the serial number
input: const Item& other - the other item for comparison
output: bool - compare result
*/
bool Item::operator<(const Item & other) const
{
	return (this->_serialNumber < other._serialNumber);
}

/*
Operator > - perform the operator on the string of the serial number
input: const Item& other - the other item for comparison
output: bool - compare result
*/
bool Item::operator>(const Item & other) const
{
	return (this->_serialNumber > other._serialNumber);
}

/*
Operator == - check if the strings are equal
input: const Item& other - the other item for comparison
output: bool - compare result
*/
bool Item::operator==(const Item & other) const
{
	return (this->_serialNumber.compare(other._serialNumber) == 0); //check if the strings are equal
}

/*
Getter - get the name field
input: none
output: none
*/
string Item::getName()
{
	return _name;
}

/*
Getter - get the serial number field
input: none
output: none
*/
string Item::getSerial()
{
	return _serialNumber;
}

/*
Getter - get the count field
input: none
output: none
*/
int Item::getCount()
{
	return _count;
}

/*
Getter - get the name field
input: none
output: none
*/
double Item::getPrice()
{
	return _unitPrice;
}

/*
Setter - set the name field
input: String - the new name of this item
output: none
*/
void Item::setName(string newName)
{
	this->_name = newName;
}

/*
Setter - set the serial field
input: String - the new serial number
output: none
*/
void Item::setSerial(string newSerial)
{
	this->_serialNumber = newSerial;
}

/*
Setter - set the count field
input: String - the new count for this item
output: none
*/
void Item::setCount(int newCount)
{
	if (newCount < 0)
	{
		std::cout << "Count cannot be negative" << std::endl;
	}
	else
	{
		this->_count = newCount;
	}
}

/*
Setter - set the unitPrice field
input: String - the new price
output: none
*/
void Item::setPrice(int newPrice)
{
	//update the unit price only if bigger than zero
	if (newPrice > 0)
	{
		this->_unitPrice = newPrice;
	}
	else
	{
		std::cout << "Price must be positive" << std::endl << "Remember, you must always stay positive" << std::endl;
	}
}
